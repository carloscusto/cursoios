//
//  MenuView.swift
//  CursoIOS
//
//  Created by CarlosCusto on 12/13/21.
//

import SwiftUI

struct MenuView: View {
    var body: some View {
        NavigationView {
            List {
                NavigationLink("Pantalla Textos",
                               destination: TextosView())
                NavigationLink("Pantalla Imagenes",
                               destination: ImagenesView())
                Text("Otra pantalla")
            }
            .navigationTitle("Menu")
            .navigationBarTitleDisplayMode(.automatic)
            .navigationBarHidden(false)
        }
    }
}

struct MenuView_Previews: PreviewProvider {
    static var previews: some View {
        MenuView()
    }
}
