//
//  PropertyWrappersView.swift
//  CursoIOS
//
//  Created by CarlosCusto on 12/13/21.
//

import SwiftUI

struct PropertyWrappersView: View {
    
    @State var contador: Int = 0
    
    var body: some View {
        VStack {
            Text("Contador: \(contador)")
            Button("Incrementar Valor") {
                self.contador += 1
            }
        }
    }
}

struct PropertyWrappersView_Previews: PreviewProvider {
    static var previews: some View {
        PropertyWrappersView()
    }
}
