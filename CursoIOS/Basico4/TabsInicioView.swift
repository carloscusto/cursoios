//
//  TabsInicioView.swift
//  CursoIOS
//
//  Created by CarlosCusto on 12/13/21.
//

import SwiftUI

struct TabsInicioView: View {
    var body: some View {
        TabView {
            HomeView()
                .tabItem {
                    Image(systemName: "house.fill")
                    Text("Inicio")
                }
            ProfileView()
                .tabItem {
                    Image(systemName: "person.crop.circle.fill")
                    Text("Perfil")
                }
        }.accentColor(.blue)
    }
}

struct TabsInicioView_Previews: PreviewProvider {
    static var previews: some View {
        TabsInicioView()
    }
}
