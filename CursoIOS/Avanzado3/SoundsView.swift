//
//  SoundsView.swift
//  CursoIOS
//
//  Created by CarlosCusto on 12/16/21.
//

import SwiftUI
import AVKit


struct SoundModel: Hashable {
    let name: String
 
    func getURL() -> URL {
        return URL(string: Bundle.main.path(forResource: name, ofType: "wav")!)!
    }
}


let arrayOfSounds: [SoundModel] = [
    .init(name: "1"),
    .init(name: "2"),
    .init(name: "3"),
    .init(name: "4"),
    .init(name: "5")
]


class SoundPlayer {
    var player: AVAudioPlayer?
 
    func play(withURL url: URL) {
        player = try! AVAudioPlayer(contentsOf: url)
        player?.prepareToPlay()
        player?.play()
    }
}


struct SoundsView: View {
    private let soundPlayer = SoundPlayer()
    
    var body: some View {
        List {
            ForEach(arrayOfSounds, id: \.self) { sound in
                Button("Play Sound \(sound.name)") {
                    soundPlayer.play(withURL: sound.getURL())
                }
            }
        }
    }
}

struct SoundsView_Previews: PreviewProvider {
    static var previews: some View {
        SoundsView()
    }
}
