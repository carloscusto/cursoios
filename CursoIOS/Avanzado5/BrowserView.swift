//
//  BrowserView.swift
//  CursoIOS
//
//  Created by CarlosCusto on 12/16/21.
//

import SwiftUI

struct BrowserView: View {
    
    @StateObject var model = WebViewModel()

    var body: some View {
        ZStack(alignment: .bottom) {
            Color.black
                .ignoresSafeArea()
            VStack(spacing: 0) {
                HStack(spacing: 10) {
                    HStack {
                        TextField("Ingresa una url",
                                    text: $model.urlString)
                            .keyboardType(.URL)
                            .autocapitalization(.none)
                            .padding(10)
                        Spacer()
                    }
                    .background(Color.white)
                    .cornerRadius(30)
                    Button("Ir", action: {
                        model.loadUrl()
                    })
                    .foregroundColor(.white)
                    .padding(10)
                }.padding(10)
                ZStack {
                    WebView(webView: model.webView)
                    
                    if model.isLoading {
                        ProgressView()
                            .progressViewStyle(CircularProgressViewStyle())
                    }
                }
            }
        }
        .toolbar {
            ToolbarItemGroup(placement: .bottomBar) {
                Button(action: {
                    model.goBack()
                }, label: {
                    Image(systemName: "arrowshape.turn.up.backward")
                })
                .disabled(!model.canGoBack)
                        
                Button(action: {
                    model.goForward()
                }, label: {
                    Image(systemName: "arrowshape.turn.up.right")
                })
                .disabled(!model.canGoForward)
                        
                Spacer()
            }
        }
    }
}

struct BrowserView_Previews: PreviewProvider {
    static var previews: some View {
        BrowserView()
    }
}
