//
//  WebView.swift
//  CursoIOS
//
//  Created by CarlosCusto on 12/16/21.
//

import SwiftUI
import WebKit


struct WebView: UIViewRepresentable {
    typealias UIViewType = WKWebView
    let webView: WKWebView
 
    func makeUIView(context: Context) -> WKWebView {
        return webView
    }
 
    func updateUIView(_ uiView: WKWebView, context: Context) { }
}
