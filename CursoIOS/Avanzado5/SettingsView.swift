//
//  SettingsView.swift
//  CursoIOS
//
//  Created by CarlosCusto on 12/16/21.
//

import SwiftUI

struct SettingsView: View {
    
    @AppStorage("nombresUser") var nombres: String = ""
    @AppStorage("apellidosUser") var apellidos: String = ""
    @AppStorage("esMiembroUser") var esMiembro: Bool = false
       
    var body: some View {
        NavigationView {
            VStack {
                Form {
                    Section(header: Text("Datos personales")) {
                        TextField("Nombres", text: $nombres)

                        TextField("Apellidos", text: $apellidos)
                    }
                    
                    Section(header: Text("Membresía")) {
                        Toggle("Miembro suscripto", isOn: $esMiembro)
                    }
                }
            }
            .navigationTitle("Configuración")
        }
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView()
    }
}
