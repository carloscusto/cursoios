//
//  WebView.swift
//  CursoIOS
//
//  Created by CarlosCusto on 12/16/21.
//

import Foundation
import WebKit
import Combine


class WebViewModel: ObservableObject {
    let webView: WKWebView
 
    init() {
        webView = WKWebView(frame: .zero)
        setupBindings()
    }
    
    // outputs
    @Published var canGoBack: Bool = false
    @Published var canGoForward: Bool = false
    @Published var isLoading: Bool = false
    
    private func setupBindings() {
        webView.publisher(for: \.canGoBack)
            .assign(to: &$canGoBack)
            
        webView.publisher(for: \.canGoForward)
            .assign(to: &$canGoForward)
        
        webView.publisher(for: \.isLoading)
            .assign(to: &$isLoading)
    }
    
    // inputs
    @Published var urlString: String = ""

    // actions
    func loadUrl() {
        guard let url = URL(string: urlString) else {
            return
        }
            
        webView.load(URLRequest(url: url))
    }
        
    func goForward() {
        webView.goForward()
    }
        
    func goBack() {
        webView.goBack()
    }

}
