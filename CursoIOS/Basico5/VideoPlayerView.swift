//
//  VideoPlayerView.swift
//  CursoIOS
//
//  Created by CarlosCusto on 12/13/21.
//

import SwiftUI
import AVKit

struct VideoPlayerView: View {
    
    let url = URL(string: "https://media1.giphy.com/media/NWlBEcDW5evFS/giphy.mp4")!
    
    var body: some View {
        VStack {
            VideoPlayer(player: .init(url: url)) {
                Text("👍 Si te ha gustado\npuedes dar like al video")
                    .bold()
                    .foregroundColor(.white)
                Spacer()
            }
            Text("▶️ ¡Suscríbete y apoya el canal!")
                .bold()
                .padding(60)
        }
        .ignoresSafeArea()
    }
}

struct VideoPlayerView_Previews: PreviewProvider {
    static var previews: some View {
        VideoPlayerView()
    }
}
