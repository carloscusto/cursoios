//
//  LocationViewModel.swift
//  CursoIOS
//
//  Created by CarlosCusto on 12/16/21.
//

import Foundation
import CoreLocation
import MapKit


final class LocationViewModel: NSObject, ObservableObject {

    private struct Span {
        static let delta = 0.1
    }

    private let locationManager: CLLocationManager = .init()
    @Published var userLocation: MKCoordinateRegion = .init()
    @Published var userLocation2: CLLocation = .init()
    
    override init() {
        super.init()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
    }
}

extension LocationViewModel: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager,
                         didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
 
        print("Location \(location)")
        userLocation = .init(center: location.coordinate,
                                span: .init(latitudeDelta: Span.delta, longitudeDelta: Span.delta))
        userLocation2 = location
    }
}
