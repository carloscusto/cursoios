//
//  MapaView.swift
//  CursoIOS
//
//  Created by CarlosCusto on 12/16/21.
//

import SwiftUI
import MapKit


struct MapaView: View {
    
    @StateObject var locationViewModel = LocationViewModel()

    var body: some View {
        Map(coordinateRegion: $locationViewModel.userLocation, showsUserLocation: true)
            .ignoresSafeArea()
    }
}

struct MapaView_Previews: PreviewProvider {
    static var previews: some View {
        MapaView()
    }
}
