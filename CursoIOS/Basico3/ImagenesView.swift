//
//  ImagenesView.swift
//  CursoIOS
//
//  Created by CarlosCusto on 12/12/21.
//

import SwiftUI

struct ImagenesView: View {
    var body: some View {
        VStack {
            Text("Imagen agregada vía assets")
            Image("logo_conecta").resizable().frame(width:300, height: 100)

            Divider().background(.black)
            
            Text("Usando los simbolos del sistema").padding()
            HStack {
                Image(systemName: "person.fill").resizable().frame(width:50, height: 50).foregroundColor(.blue)

                Image(systemName: "heart.fill").resizable().frame(width:50, height: 50).foregroundColor(.red)
            }
        }
    }
}

struct ImagenesView_Previews: PreviewProvider {
    static var previews: some View {
        ImagenesView()
    }
}
