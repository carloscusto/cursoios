//
//  TextosView.swift
//  CursoIOS
//
//  Created by CarlosCusto on 12/12/21.
//

import SwiftUI

struct TextosView: View {
    var body: some View {
        VStack {
            Text("Bienvenidos al Curso de iOS")
                .font(.title)
                .padding()
            Text("Este es un ejemplo de como agregar textos")
                .font(.title2)
                .padding()
            Text("Textos apilados verticalmente en un VStack")
                .font(.largeTitle)
                .foregroundColor(.red)
                .padding()
        }
    }
}

struct TextosView_Previews: PreviewProvider {
    static var previews: some View {
        TextosView()
    }
}
