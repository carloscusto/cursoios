import UIKit

var greeting = "Hello, playground"

var nombrevariable = "Una cadena de texto o string"
nombrevariable = "nuevo valor"
nombrevariable = "nuevamente cambia valor"

let constante = "Un valor que no se puede cambiar"
//constante = "esto produciría un error"

var variable2 = 50
var variable3 = 20.8

var variableExplicita2:Int = 50
var variableExplicita3:Float = 20.8

print("Esto aparece en debug área")

