import UIKit

print("Esto aparece en debug área")

func nombreFuncion() {
    print("Se ejecutó la función")
}
nombreFuncion()

func suma(x:Int, y:Int){
    print(x+y)
}
suma(x:10, y:10)

func sumaConRetorno(x:Int, y:Int) -> Int {
    return x+y
}

var resultado = sumaConRetorno(x: 20, y: 30)
