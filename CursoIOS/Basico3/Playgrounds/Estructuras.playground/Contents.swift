import UIKit

struct User {
    var nombre: String
    var edad: Int
}

var user1 = User(nombre:"Jose", edad:20)
user1.nombre

var user2 = user1
user2.nombre = "Pedro"
user2.nombre
user1.nombre
