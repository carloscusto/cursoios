import UIKit

class User {
    var nombre: String
    var edad: Int
    
    init(nombre: String, edad: Int) {
        self.nombre = nombre
        self.edad = edad
    }
}

var user1 = User(nombre:"Jose", edad:20)
user1.nombre

var user2 = user1
user2.nombre = "Pedro"
user2.nombre
user1.nombre
