//
//  ContentView.swift
//  CursoIOS
//
//  Created by CarlosCusto on 12/12/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hola Mundo!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
