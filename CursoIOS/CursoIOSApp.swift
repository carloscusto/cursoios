//
//  CursoIOSApp.swift
//  CursoIOS
//
//  Created by CarlosCusto on 12/12/21.
//

import SwiftUI

@main
struct CursoIOSApp: App {
    @Environment(\.scenePhase) var scenePhase
    
    init() {
        print("La aplicación está iniciando.")
    }
    
    var body: some Scene {
        WindowGroup {
            SettingsView()
        }
        .onChange(of: scenePhase) { newScenePhase in
            switch newScenePhase {
                case .active:
                    print("App está activa")
                case .inactive:
                    print("App está inactiva")
                case .background:
                    print("App está en background")
                @unknown default:
                    print("Oh - interesante: Recibimos un valor no esperado")
            }
        }
    }
}
