//
//  WeatherViewModel.swift
//  CursoIOS
//
//  Created by CarlosCusto on 12/15/21.
//

import Foundation


class WeatherViewModel: ObservableObject {
    @Published var weather: Weather = .empty
    private let weatherModelMapper: WeatherModelMapper = WeatherModelMapper()
    private let locationViewModel: LocationViewModel = LocationViewModel()
    private let API_KEY = "f9831154b48c55ca369828fc680f550b"
    
    func getWeather(city: String) async {
        let url = URL(string: "https://api.openweathermap.org/data/2.5/weather?lat=\(self.locationViewModel.userLocation2.coordinate.latitude)&lon=\(self.locationViewModel.userLocation2.coordinate.longitude)&appid=\(self.API_KEY)&units=metric&lang=es")!
        
        do {
            async let (data, _) = try await URLSession.shared.data(from: url)
            let dataModel = try! await JSONDecoder().decode(WeatherResponseDataModel.self, from: data)
            print(dataModel)
            
            DispatchQueue.main.async {
                self.weather = self.weatherModelMapper.mapDataModelToModel(dataModel: dataModel)
                print(self.weather)
            }
        } catch {
            print(error.localizedDescription)
        }
    }

}
