//
//  WeatherView.swift
//  CursoIOS
//
//  Created by CarlosCusto on 12/15/21.
//

import SwiftUI

struct WeatherView: View {
    
    @StateObject var viewModel = WeatherViewModel()
    
    var body: some View {
        ZStack {
            VStack {
                Text(viewModel.weather.city)
                    .font(.largeTitle)
                    .foregroundColor(.white)
                    .padding()
                HStack {
                    if let url = viewModel.weather.iconURL {
                        AsyncImage(url: url) { image in
                            image
                        } placeholder: {
                            ProgressView()
                        }
                    }
                
                    Text(viewModel.weather.temperature)
                        .font(.system(size: 70))
                        .foregroundColor(.white)
                        .padding()
                }
                Text(viewModel.weather.description)
                    .foregroundColor(.white)
                Divider()
                Spacer()
            }
            .padding(.top, 32)
        }
        .background(
            LinearGradient(colors: [.blue, .purple], startPoint: .topLeading, endPoint: .bottomTrailing)
        )
        .task {
            await viewModel.getWeather(city: "boston")
        }
    }
}

struct WeatherView_Previews: PreviewProvider {
    static var previews: some View {
        WeatherView()
    }
}
