//
//  WeatherModelMapper.swift
//  CursoIOS
//
//  Created by CarlosCusto on 12/15/21.
//

import Foundation

struct WeatherModelMapper {
    func mapDataModelToModel(dataModel: WeatherResponseDataModel) -> Weather {
        guard let weather = dataModel.weather.first else { return .empty }

        let temperature = dataModel.main
        
        return Weather(city: dataModel.name,
                       weather: weather.main,
                       description: "\(weather.description)",
                       iconURL: URL(string: "https://openweathermap.org/img/wn/\(weather.iconURLString)@2x.png"),
                       temperature: "\(Int(temperature.temp)) °C"
        )
    }
}
