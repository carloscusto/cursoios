//
//  WeatherService.swift
//  CursoIOS
//
//  Created by CarlosCusto on 12/15/21.
//

import Foundation


struct WeatherResponseDataModel: Decodable {
    let name: String
    let main: APIMain
    let weather: [APIWeather]
}

struct APIMain: Decodable {
    let temp: Double
}

struct APIWeather: Decodable {
    let main: String
    let description: String
    let iconURLString: String
    
    enum CodingKeys: String, CodingKey {
        case main
        case description
        case iconURLString = "icon"
    }
}
