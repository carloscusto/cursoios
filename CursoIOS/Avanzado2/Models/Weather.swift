//
//  Weather.swift
//  CursoIOS
//
//  Created by CarlosCusto on 12/15/21.
//

import Foundation

public struct Weather {
    let city: String
    let weather: String
    let description: String
    let iconURL: URL?
    let temperature: String
    
    static let empty: Weather = .init(city: "No city",
                                    weather: "No weather",
                                    description: "No description",
                                    iconURL: nil,
                                    temperature: "0")
}
